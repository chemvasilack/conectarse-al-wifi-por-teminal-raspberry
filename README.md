**CONECTARSE A UNA RED WI-FI CON COMANDOS**

Primero comience por verificar el nombre de tu dispositivo de red usando el siguiente comando. De la salida de este comando, el nombre/interfaz del dispositivo es wlan0 como se muestra.

-iw dev

A continuación, verifica el estado de la conexión del dispositivo Wi-Fi con el siguiente comando.

-iw wlan0 link

De la salida anterior, el dispositivo no está conectado a ninguna red, ejecuta el siguiente comando para explorar las redes Wi-Fi disponibles.

-sudo iw wlan0 scan
-sudo iwlist wlan0 scan | grep ESSID

